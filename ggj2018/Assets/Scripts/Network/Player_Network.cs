﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class Player_Network : NetworkBehaviour {

    public GameObject m_Player = null;

    private NetworkInstanceId m_NetID;

    private void Start()
    {
        if(isLocalPlayer)
        {
            m_NetID = GetComponent<NetworkIdentity>().netId;
            m_Player.SetActive(true);
            CmdSignalPresence(m_NetID);
            CmdRequestPlacement();
        }
    }

    [Command]
    private void CmdSignalPresence(NetworkInstanceId iNetID)
    {
        GameManager.instance.AddPlayer(iNetID);
    }

    [Command]
    private void CmdRequestPlacement()
    {
        Vector3 newPos = GameManager.instance.GetPosForPlayer();
        RpcPlaceClientAtPos(newPos.x, newPos.y, newPos.z); 
    }

    [ClientRpc]
    private void RpcPlaceClientAtPos(float x, float y, float z)
    {
        transform.position = new Vector3(x, y, z);
    }
}
