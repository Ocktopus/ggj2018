﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckCharManager : MonoBehaviour {

	public bool highlighted = false;
	public LocalPlayerManager player_manager;
		
	// Update is called once per frame
	void Update () {
		if (highlighted && Input.GetMouseButtonUp (0)) {
			player_manager.DrawCharCard ();
		}
	}

	void OnMouseEnter() {
		highlighted = true;
		transform.localScale *=  0.95f;
	}

	void OnMouseExit() {
		highlighted = false;
		transform.localScale /=  0.95f;
	}
}
