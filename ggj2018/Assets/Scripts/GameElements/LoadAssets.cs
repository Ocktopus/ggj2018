﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAssets : MonoBehaviour {

	public Texture exitTexture;

	public Texture[] room_textures;
	public Texture[] path_textures;

	public Sprite[] room_sprites;
	public Sprite[] path_sprites;

	public Texture[] card_char;
	public Texture[] card_world;

}
