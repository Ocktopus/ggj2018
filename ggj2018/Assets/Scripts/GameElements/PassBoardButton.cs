﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassBoardButton : MonoBehaviour {

	public bool highlighted = false;
	public bool clicked = false;

	public Vector3 base_state;
	public Vector3 hovering_state;
	public Vector3 clicked_state;

	public Color btn_natural;
	public Color btn_hovered;
	public Color btn_pressed;

	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().material.color = btn_natural;		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonUp (0)) {
			if (clicked) {
				transform.localScale = base_state;	
				GetComponent<Renderer>().material.color = btn_natural;
				clicked = false;

				// CANCEL PASS BOARD

			}
		}
	}

	void OnMouseExit() {
		if (!clicked) {
			highlighted = false;
			// transform.localScale =  base_state;	
			GetComponent<Renderer>().material.color = btn_natural;
		}
	}

	void OnMouseOver() {
		if (Input.GetMouseButton (0)) {
			transform.localScale = clicked_state;
			GetComponent<Renderer>().material.color = btn_pressed;	
			clicked = true;
			highlighted = false;
		} 
		else if(Input.GetMouseButtonUp (0)) {			
			transform.localScale = base_state;
			GetComponent<Renderer>().material.color = btn_natural;	
			clicked = false;

            //PASS BOARD

        }
		else if(!clicked) {
			highlighted = true;
			//transform.localScale = hovering_state;	
			GetComponent<Renderer>().material.color = btn_hovered;
		}
	}
}
