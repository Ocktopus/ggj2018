﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCard : Card {

	public bool highlighted = false;
	public GameObject fleche;

	void Start() {
		fleche = GameObject.Find("RotateArrows");
		fleche = fleche.transform.GetChild(0).gameObject;
	}
	
	void Update () {
		if (highlighted && Input.GetMouseButtonUp (0)) {
			fleche.SetActive(true);
		}
	}
	void OnMouseEnter() {
		highlighted = true;
		transform.localScale *=  0.95f;
	}

	void OnMouseExit() {
		highlighted = false;
		transform.localScale /=  0.95f;
	}
}
