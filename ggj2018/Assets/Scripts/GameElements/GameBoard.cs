﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameBoard : NetworkBehaviour {

	public int dim_x = 5;
	public int dim_y = 5;
	public float offset;

	public GameObject prefab_tile_path;
	public GameObject prefab_tile_room;
	public GameObject prefab_tile_exit;
	public GameObject[,] board;

	// DEBUG VARIABLES
	public GameObject prefab_pawn;
	public bool reset_board = false;
	public GameObject pawn;
	public GameObject prefab_trap;


    //Network
    public StructGameBoard m_BoardStruct;
    [ClientRpc]
    public void RpcDoSomething(int i)
    {
        Debug.Log(i);
    }

    [ClientRpc]
    public void RpcRecieveNewBoard(StructGameBoard iNewBoard)
    {
        CopyFrom(iNewBoard);
    }

    private void CopyFrom(StructGameBoard iNewBoard)
    {
        m_BoardStruct.tiles = iNewBoard.tiles;

        for(int i = 0; i < 5; i++)
        {
            for(int j = 0; j < 5; j++)
            {
                if(!board[i, j].GetComponent<Tiles>())
                {
                    Debug.Log(i + " : " + j);
                }
                Destroy(board[i, j].GetComponent<Tiles>());
                Tiles.InstanciateTileOnGameObject(iNewBoard.tiles[i * 5 + j], board[i, j]);
            }
        }
    }

    private void Awake()
    {
        m_BoardStruct = new StructGameBoard();
        m_BoardStruct.tiles = new StructTile[25];
        m_BoardStruct.player_pos = new Vector2();
    }

    // Use this for initialization
    void Start () {		
		dim_x = 5;
		dim_y = 5;

		board = new GameObject[dim_x, dim_y];
		InitializeBoard (); // TODO : A faire faire par le serveur et à synchroniser avec les joueurs à l'initialisation
	}
	
	// Update is called once per frame
	void Update () {

		Debug.Log (m_BoardStruct.player_pos);

		// DEBUG -- RESET BOARD
		if (reset_board) {
			reset_board = false;
			for (int x = 0; x < dim_x; x++) {
				for (int y = 0; y < dim_y; y++) {
					Destroy(board[x, y]);
				}
			}
			Destroy (pawn);
			InitializeBoard();

        }
	}

	public void InitializeBoard() {

        // Create Board
        dim_x = 5;
        dim_y = 5;
        // board = new GameObject[dim_x, dim_y];
        for (int x = 0; x < dim_x; x++) {
			for (int y = 0; y < dim_y; y++) {
				
				int tile_type_random = Random.Range (0, 2); // TODO : A ponderer si on souhaite (-) de 'room' et (+) de 'path'

				if (tile_type_random == 0) {					
					board [x, y] = Instantiate (prefab_tile_room, new Vector3 (x + x * offset, 0, y + y * offset), Quaternion.identity, transform);
					board [x, y].GetComponent<Tiles_Room> ().GenerateRoom ();
				} 
				else {
					board [x, y] = Instantiate (prefab_tile_path, new Vector3 (x + x * offset, 0, y + y * offset), Quaternion.identity, transform);
					board [x, y].GetComponent<Tiles_Path> ().GeneratePath ();
				}

				board [x, y].GetComponent<Tiles> ().x = x;
				board [x, y].GetComponent<Tiles> ().y = y;
			}		
		}

		// Place Exit
		int rand_exit_x = Random.Range(0, dim_x);
		int rand_exit_y = Random.Range(0, dim_y);
		Destroy(board [rand_exit_x, rand_exit_y]);

		board [rand_exit_x, rand_exit_y] = Instantiate (prefab_tile_exit, new Vector3 (rand_exit_x + rand_exit_x * offset, 0, rand_exit_y + rand_exit_y * offset), Quaternion.identity, transform);
		board [rand_exit_x, rand_exit_y].GetComponent<Tiles_Exit>().GenerateExit();
		board [rand_exit_x, rand_exit_y].GetComponent<Tiles> ().x = rand_exit_x;
		board [rand_exit_x, rand_exit_y].GetComponent<Tiles> ().y = rand_exit_y;
        // Place Player
        bool player_placed = false;
		while (!player_placed) {

			int player_x = Random.Range (0, dim_x);
			int player_y = Random.Range (0, dim_y);

			// TODO : A modifier pour éviter de spawn trop proche de la sortie
			if (player_x != rand_exit_x && player_y != rand_exit_y) {
				player_placed = true;
                m_BoardStruct.player_pos = new Vector2 (player_x, player_y);
				pawn = Instantiate (prefab_pawn, new Vector3 (player_x + player_x * offset, 0.2f, player_y + player_y * offset), Quaternion.identity);
				pawn.transform.SetParent (board [player_x, player_y].transform);
			}
		}
        InitializeBoardStruct();

    }

    public void InitializeBoardStruct()
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                m_BoardStruct.tiles[i * 5 + j] = StructTileFrom(board[i, j].GetComponent<Tiles>());
            }
        }
    }

    private StructTile StructTileFrom(Tiles iTile)
    {
        StructTile tileStruct = new StructTile();
        tileStruct.x = iTile.x;
        tileStruct.y = iTile.y;
        tileStruct.tileShape = iTile.m_Shape;
        tileStruct.tileType = iTile.my_tile_type;
        tileStruct.isTrap = iTile.isTrap;
        tileStruct.my_openings = iTile.my_openings;
        tileStruct.rotation = iTile.m_RotationNumber;

        return tileStruct;
    }

    public void GetChildrenTiles()
    {
        board = new GameObject[5,5];
        for(int i = 0; i < transform.childCount; i++)
        {
            board[i / 5, i % 5] = transform.GetChild(i).gameObject;
        }
    }
}
