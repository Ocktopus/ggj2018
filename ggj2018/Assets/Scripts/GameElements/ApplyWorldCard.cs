﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyWorldCard : MonoBehaviour {

	public GameObject[] prefabs;
	public enum TypeOfCard{REPLACE,SLIDE,TRAP};
	public TypeOfCard type;
	public GameBoard gameboard;

	private GameObject cardObject;

	public void pickCard(){
		type = (TypeOfCard)Random.Range(0,System.Enum.GetNames(typeof(TypeOfCard)).Length);
		this.gameboard = gameObject.GetComponent<GameBoard>();
		ApplyCard(type);
	}


	void ApplyCard(TypeOfCard type){
		
		switch (type)
		{
			case TypeOfCard.REPLACE:{
				cardObject = Instantiate(prefabs[0],new Vector3(1,0,-7),Quaternion.Euler(0,180,0));
				ReplaceWorldCard card = cardObject.GetComponent<ReplaceWorldCard>();
				card.m_Board = gameboard;
				card.GenerateReplaceCard();
				if(((card.m_LineNumber != gameboard.m_BoardStruct.player_pos.x) && (card.m_ColumnNumber != gameboard.m_BoardStruct.player_pos.y)) && !(gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Exit>())){
					Destroy(gameboard.board[card.m_LineNumber,card.m_ColumnNumber]);
					switch (card.m_TypeOfTile)
					{
						case Card.TypeOfTile.ROOM :{
							gameboard.board[card.m_LineNumber,card.m_ColumnNumber] = Instantiate(gameboard.prefab_tile_room, new Vector3 (card.m_LineNumber + card.m_LineNumber * gameboard.offset, 0, card.m_ColumnNumber + card.m_ColumnNumber * gameboard.offset), Quaternion.identity);
							switch (card.m_Type)
							{
								case Card.TypeOfPath.STRAIGHT:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Room>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.STRAIGHT,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.T:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Room>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.T_SHAPE,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.CROSS:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Room>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.CROSS,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.TURN:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Room>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.TURN,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.DEAD_END:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Room>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.DEAD_END,card.m_Rotation);
									break;
								}
							}
							break;
						}
						case Card.TypeOfTile.PATH :{
							gameboard.board[card.m_LineNumber,card.m_ColumnNumber] = Instantiate(gameboard.prefab_tile_path, new Vector3 (card.m_LineNumber + card.m_LineNumber * gameboard.offset, 0, card.m_ColumnNumber + card.m_ColumnNumber * gameboard.offset), Quaternion.identity);
							switch (card.m_Type)
							{
								case Card.TypeOfPath.STRAIGHT:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Path>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.STRAIGHT,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.T:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Path>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.T_SHAPE,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.CROSS:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Path>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.CROSS,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.TURN:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Path>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.TURN,card.m_Rotation);
									break;
								}
								case Card.TypeOfPath.DEAD_END:{
									gameboard.board[card.m_LineNumber,card.m_ColumnNumber].GetComponent<Tiles_Path>().SetupRoom(card.m_LineNumber,card.m_ColumnNumber,Tiles.TileType.DEAD_END,card.m_Rotation);
									break;
								}
							}	
							break;
						}
					}
				}
				break;
			}
			case TypeOfCard.SLIDE:{
				cardObject = Instantiate(prefabs[1],new Vector3(1,0,-7),Quaternion.Euler(0,180,0));
				SlideWorldCard card = cardObject.GetComponent<SlideWorldCard>();
				card.m_Board = gameboard;
				card.GenerateSlideCard();
				Slide(card.m_direction, card.m_LineColumnNumber);
				break;
			}
			case TypeOfCard.TRAP:{
				cardObject = Instantiate(prefabs[2],new Vector3(1,0,-7),Quaternion.Euler(0,180,0));
				TrapCard card = cardObject.GetComponent<TrapCard>();
				card.m_Board = gameboard;
				card.GenerateTrapCard();
				if(card.m_RoomTrapped.GetComponent<Tiles_Room>()){
					card.m_RoomTrapped.GetComponent<Tiles_Room>().isTrap = true;
				}
				else if(card.m_RoomTrapped.GetComponent<Tiles_Path>()){
					card.m_RoomTrapped.GetComponent<Tiles_Path>().isTrap = true;
				}
				break;
			}
		}
		Destroy (cardObject);
	}

	public void Slide(Card.Direction direction, int lineColumnNumber){
		switch (direction)
						{
							case Card.Direction.LEFT:{
								GameObject temp = gameboard.board[gameboard.dim_x-1,lineColumnNumber];
								for(int i=gameboard.dim_x-1;i>=0;i--){	
									if (i<=0)
									{
										gameboard.board[0,lineColumnNumber] = temp;
									}
									else{
										gameboard.board[i,lineColumnNumber] = gameboard.board[i-1,lineColumnNumber];
									}
									gameboard.board[i,lineColumnNumber].transform.position = new Vector3(i + i*gameboard.offset,0,lineColumnNumber + lineColumnNumber*gameboard.offset);
									if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
								}
								if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.y){
									gameboard.m_BoardStruct.player_pos.x+=1;
									if (gameboard.m_BoardStruct.player_pos.x > 4) {
										gameboard.m_BoardStruct.player_pos.x = 0;
									}
								}
								break;
							}
							case Card.Direction.RIGHT:{
								GameObject temp = gameboard.board[0,lineColumnNumber];
								for(int i=0;i<gameboard.dim_x;i++){
									if (i>=(gameboard.dim_x -1)){
										gameboard.board[gameboard.dim_x -1,lineColumnNumber] = temp;
									}
									else{
										gameboard.board[i,lineColumnNumber] = gameboard.board[i+1,lineColumnNumber];
									}
									gameboard.board[i,lineColumnNumber].transform.position = new Vector3(i + i*gameboard.offset,0,lineColumnNumber + lineColumnNumber*gameboard.offset);
									if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
								}
								if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.y){
									gameboard.m_BoardStruct.player_pos.x-=1;
									if (gameboard.m_BoardStruct.player_pos.x < 0) {
										gameboard.m_BoardStruct.player_pos.x = 4;
									}
								}
								break;
							}
							case Card.Direction.TOP:{
									GameObject temp = gameboard.board[lineColumnNumber,0];
									for(int i=0;i<gameboard.dim_y;i++){
										if (i>=(gameboard.dim_y-1)){
											 gameboard.board[lineColumnNumber,gameboard.dim_y-1] = temp;
										}
										else{
											gameboard.board[lineColumnNumber,i] = gameboard.board[lineColumnNumber,i+1];
										}
										gameboard.board[lineColumnNumber,i].transform.position = new Vector3(lineColumnNumber + lineColumnNumber*gameboard.offset,0,i + i*gameboard.offset);
										if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().y =(int) (i + i*gameboard.offset);
										}
									}
									if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.x){
										gameboard.m_BoardStruct.player_pos.y-=1;
										if (gameboard.m_BoardStruct.player_pos.y < 0) {
											gameboard.m_BoardStruct.player_pos.y = 4;
										}
									}	
									break;
								}
								case Card.Direction.BOTTOM:{
									GameObject temp = gameboard.board[lineColumnNumber,gameboard.dim_y-1];
									for(int i=gameboard.dim_y-1;i>=0;i--){
										if (i<=0){
											gameboard.board[lineColumnNumber,0] = temp;
										}
										else{
											gameboard.board[lineColumnNumber,i] = gameboard.board[lineColumnNumber,i-1];
										}
										gameboard.board[lineColumnNumber,i].transform.position = new Vector3(lineColumnNumber + lineColumnNumber*gameboard.offset,0,i + i*gameboard.offset);
										if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().y =(int) (i + i*gameboard.offset);
										}
									}
									if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.x){
										gameboard.m_BoardStruct.player_pos.y+=1;
										if (gameboard.m_BoardStruct.player_pos.y > 4) {
											gameboard.m_BoardStruct.player_pos.y = 0;
										}
									}		
									break;
								}
						}
	}
}
