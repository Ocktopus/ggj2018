﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideWorldCard : SlideCard {

    public GameBoard m_Board;
    public Direction m_direction;
    public int m_LineColumnNumber;

    public new void GenerateSlideCard(){
        base.GenerateSlideCard();
        switch (m_LineColumn)
        {
            case LineColumn.LINE:{
                m_LineColumnNumber = Random.Range(0,m_Board.dim_x);
                m_direction = (Direction)Random.Range(2,4);
                break;
            }
            case LineColumn.COLUMN:{
                m_LineColumnNumber = Random.Range(0,m_Board.dim_y);
                m_direction = (Direction) Random.Range(0,2);
                break;
            }
        }
        
    }
}
