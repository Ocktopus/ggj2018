﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideButton : MonoBehaviour {

	public GameBoard board;

	public Card.Direction direction;
	public int lineColumnNumber;
	private ApplyWorldCard card;
	private Button btn;
	public LocalPlayerManager player_manager;

	// Use this for initialization
	void Start () {
		player_manager = GameObject.Find ("LocalPlayer").GetComponent<LocalPlayerManager>();
		card = board.GetComponent<ApplyWorldCard>();
		btn = gameObject.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
	}
	
	// Update is called once per frame
	void TaskOnClick () {
		card.Slide(direction, lineColumnNumber);
		player_manager.waiting_for_slide = false;
		btn.gameObject.transform.parent.gameObject.SetActive(false);
	}
}
