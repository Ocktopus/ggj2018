﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideCard : Card {
	public LineColumn m_LineColumn;
	LoadAssets load_assets;
	public bool highlighted = false;
	public LocalPlayerManager player_manager;

	public void Start(){	
		player_manager = GameObject.Find ("LocalPlayer").GetComponent<LocalPlayerManager>();
		load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();	
    }

	public void SetDirection(int dir) {
		load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();	
		// O : Line // 1 : Row
		if (dir == 0) {
			transform.Rotate (new Vector3 (0, 180, 0));
			m_LineColumn = LineColumn.LINE;
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.card_world[0]);
		} else {
			m_LineColumn = LineColumn.COLUMN;
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.card_world[1]);
		}
	}

	public void GenerateSlideCard(){
        m_LineColumn = (LineColumn)Random.Range(0,System.Enum.GetNames(typeof(LineColumn)).Length);
    }

	void Update () {
		if (highlighted && Input.GetMouseButtonUp (0) && !card_played && !player_manager.waiting_for_slide) {
			player_manager.waiting_for_slide = true;
			player_manager.card_played = true;
			card_played = true;

			switch (m_LineColumn) {
			case LineColumn.COLUMN:
				player_manager.vertical_slide_canvas.SetActive (true);
				break;
			case LineColumn.LINE:
				player_manager.horizontal_slide_canvas.SetActive (true);				
				break;
			default:
				break;
			}

			transform.localScale = new Vector3 (0.2849f, 2.5763f, 0.2836f);
			transform.position = new Vector3 (6.63f, 0.4367f, 1.95f);
		}

		if (card_played && !player_manager.waiting_for_slide) {
			player_manager.card_played = false;
			Destroy (this.gameObject);
		}
	}

	void OnMouseEnter() {
		if (!player_manager.card_played) {
			highlighted = true;
			transform.localScale *=  0.95f;
		}
	}

	void OnMouseExit() {
		if (!player_manager.card_played) {
			highlighted = false;
			transform.localScale /= 0.95f;
		}
	}
}
