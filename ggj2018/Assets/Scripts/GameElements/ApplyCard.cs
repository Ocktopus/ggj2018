﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyCard : MonoBehaviour {

	public GameBoard gameboard;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Slide(Card.Direction direction, int lineColumnNumber){
		switch (direction)
						{
							case Card.Direction.LEFT:{
								GameObject temp = gameboard.board[gameboard.dim_x-1,lineColumnNumber];
								for(int i=gameboard.dim_x-1;i>=0;i--){	
									if (i<=0)
									{
										gameboard.board[0,lineColumnNumber] = temp;
									}
									else{
										gameboard.board[i,lineColumnNumber] = gameboard.board[i-1,lineColumnNumber];
									}
									gameboard.board[i,lineColumnNumber].transform.position = new Vector3(i + i*gameboard.offset,0,lineColumnNumber + lineColumnNumber*gameboard.offset);
									if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
								}
								if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.y){
									gameboard.m_BoardStruct.player_pos.x+=1;
								}
								break;
							}
							case Card.Direction.RIGHT:{
								GameObject temp = gameboard.board[0,lineColumnNumber];
								for(int i=0;i<gameboard.dim_x;i++){
									if (i>=(gameboard.dim_x -1)){
										gameboard.board[gameboard.dim_x -1,lineColumnNumber] = temp;
									}
									else{
										gameboard.board[i,lineColumnNumber] = gameboard.board[i+1,lineColumnNumber];
									}
									gameboard.board[i,lineColumnNumber].transform.position = new Vector3(i + i*gameboard.offset,0,lineColumnNumber + lineColumnNumber*gameboard.offset);
									if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Room>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Path>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
									else if(gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>()){
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().x =(int) (i + i*gameboard.offset);
										gameboard.board[i,lineColumnNumber].GetComponent<Tiles_Exit>().y =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
									}
								}
								if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.y){
									gameboard.m_BoardStruct.player_pos.x-=1;
								}
								break;
							}
							case Card.Direction.TOP:{
									GameObject temp = gameboard.board[lineColumnNumber,0];
									for(int i=0;i<gameboard.dim_y;i++){
										if (i>=(gameboard.dim_y-1)){
											 gameboard.board[lineColumnNumber,gameboard.dim_y-1] = temp;
										}
										else{
											gameboard.board[lineColumnNumber,i] = gameboard.board[lineColumnNumber,i+1];
										}
										gameboard.board[lineColumnNumber,i].transform.position = new Vector3(lineColumnNumber + lineColumnNumber*gameboard.offset,0,i + i*gameboard.offset);
										if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().y =(int) (i + i*gameboard.offset);
										}
									}
									if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.x){
										gameboard.m_BoardStruct.player_pos.y-=1;
									}	
									break;
								}
								case Card.Direction.BOTTOM:{
									GameObject temp = gameboard.board[lineColumnNumber,gameboard.dim_y-1];
									for(int i=gameboard.dim_y-1;i>=0;i--){
										if (i<=0){
											gameboard.board[lineColumnNumber,0] = temp;
										}
										else{
											gameboard.board[lineColumnNumber,i] = gameboard.board[lineColumnNumber,i-1];
										}
										gameboard.board[lineColumnNumber,i].transform.position = new Vector3(lineColumnNumber + lineColumnNumber*gameboard.offset,0,i + i*gameboard.offset);
										if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Room>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Path>().y =(int) (i + i*gameboard.offset);
										}
										else if(gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>()){
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().x =(int) (lineColumnNumber + lineColumnNumber*gameboard.offset);
											gameboard.board[lineColumnNumber,i].GetComponent<Tiles_Exit>().y =(int) (i + i*gameboard.offset);
										}
									}
									if(lineColumnNumber == gameboard.m_BoardStruct.player_pos.x){
										gameboard.m_BoardStruct.player_pos.y+=1;
									}		
									break;
								}
						}
	}
}
