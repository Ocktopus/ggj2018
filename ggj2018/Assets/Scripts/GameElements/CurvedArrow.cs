﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvedArrow : MonoBehaviour {

	public bool highlighted = false;

	public GameBoard gameboard;
	public enum Rotate{RIGHT,LEFT};
	public Rotate rot;
	

	void Update () {
		if (highlighted && Input.GetMouseButtonUp (0)) {
			switch(rot){
				case Rotate.LEFT:{
					gameboard.board[(int)gameboard.m_BoardStruct.player_pos.x,(int)gameboard.m_BoardStruct.player_pos.y].GetComponent<Tiles>().RotateTileRight();
					break;
				}
				case Rotate.RIGHT:{
					gameboard.board[(int)gameboard.m_BoardStruct.player_pos.x,(int)gameboard.m_BoardStruct.player_pos.y].GetComponent<Tiles>().RotateTileLeft();
					break;
				}
			}
			gameObject.transform.parent.gameObject.SetActive(false);
		}
	}

	void OnEnable(){
		gameObject.transform.parent.position = new Vector3(gameboard.m_BoardStruct.player_pos.x,gameObject.transform.position.y+0.5f,gameboard.m_BoardStruct.player_pos.y); 
	}
	void OnMouseEnter() {
		highlighted = true;
		transform.localScale *=  0.95f;
	}

	void OnMouseExit() {
		highlighted = false;
		transform.localScale /=  0.95f;
	}
}
