﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour {
    public static GameManager instance = null;

    public List<GameObject> m_Players = new List<GameObject>();
    public List<Transform> m_PlayersSpots = new List<Transform>();
    public List<GameObject> m_ExitButtons = new List<GameObject>();
    public List<GameBoard> m_GameBoards = new List<GameBoard>();
    public List<Transform> m_BoardSpots = new List<Transform>();
    public StructGameBoard[] m_StructBoards = new StructGameBoard[4];

    public bool ShouldTurn = false;

    private int m_PlayersNb = 0;
    private int m_CurrentTurn = 0;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        m_GameBoards[0].InitializeBoard();
        for (int i=0; i<3; i++)
        {
            m_GameBoards.Add(Instantiate<GameBoard>(m_GameBoards[0], m_BoardSpots[i].position, Quaternion.identity));
            m_GameBoards[i + 1].m_BoardStruct = m_GameBoards[0].m_BoardStruct;
            m_GameBoards[i + 1].GetChildrenTiles();
        }

        RefreshBoardStructs();

    }

    private void RefreshBoardStructs()
    {
        for (int i = 0; i < 4; i++)
        {
            m_StructBoards[i] = m_GameBoards[i].m_BoardStruct;
        }
    }

    private void Update()
    {
        if(ShouldTurn)
        {
            ShouldTurn = false;
            ChangeTurn();
        }
    }

    public void ChangeTurn()
    {
        //Send information to every clients.
        List<int> disconnectedPlayers = new List<int>();
        m_CurrentTurn++;
        m_CurrentTurn %= 4;
        for(int i=0; i<m_Players.Count; i++)
        {
            if(m_Players[i])
            {
                m_Players[i].transform.position = m_PlayersSpots[m_CurrentTurn].position;
            }
            else
            {
                disconnectedPlayers.Add(i);
            }
        }
        foreach(int i in disconnectedPlayers)
        {
            m_Players.RemoveAt(0);
        }

        if(isServer)
        {
            CmdBroadcastBoards();
        }
    }

    private void CmdBroadcastBoards()
    {
        for(int i = 0; i < 4; i++)
        {
            m_GameBoards[i].RpcRecieveNewBoard(m_StructBoards[i]);
        }
        RefreshBoardStructs();
    }

    public Vector3 GetPosForPlayer()
    {
        Vector3 newPos = m_PlayersSpots[m_PlayersNb%4].position;
        m_PlayersNb++;

        return newPos;
    }

    public void AddPlayer(NetworkInstanceId iNetID)
    {
        m_Players.Add(NetworkServer.FindLocalObject(iNetID));
    }
}
