﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCard : Card {

	public GameBoard m_Board;
	public GameObject m_RoomTrapped;
	private List<GameObject> m_Rooms = new List<GameObject>();
	private int m_Random;
	

	public void GenerateTrapCard () {
		for (int x = 0; x < m_Board.dim_x; x++) {
			for (int y = 0; y < m_Board.dim_y; y++) {
				if(!m_Board.board[x,y].GetComponent("Tiles_Exit") && !((x == m_Board.m_BoardStruct.player_pos.x) && (y == m_Board.m_BoardStruct.player_pos.y)) && !m_Board.board[x,y].GetComponent<Tiles>().isTrap){
                    m_Rooms.Add(m_Board.board[x,y]);
				}
			}
		}

		if (m_Rooms.Count > 0) {
			m_Random = Random.Range (0, m_Rooms.Count);
			m_RoomTrapped = m_Rooms [m_Random];
			GameObject trap = Instantiate (m_Board.prefab_trap);
			trap.transform.SetParent (m_RoomTrapped.transform);
			trap.transform.localPosition = Vector3.zero;
		}
	}
}
