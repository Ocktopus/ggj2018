﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiles_Exit : Tiles {

    new void Start() {
		base.Start ();
        m_Shape = TileShape.EXIT;
    }
	LoadAssets load_assets;

	public void GenerateExit() {
        m_Shape = TileShape.EXIT;
        load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();
		GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.exitTexture);
		RotateTileLeft ();
		RotateTileLeft ();
		my_openings = new Openings[] { (Openings)0, (Openings)1, (Openings)2, (Openings)3 };

	}

	new void Update() {
		base.Update ();

		if (player_manager.m_gameboard.m_BoardStruct.player_pos.x == x && player_manager.m_gameboard.m_BoardStruct.player_pos.y == y) {
			// CONGRATULATIONS
			player_manager.win_txt.SetActive(true);
		}
	
	}
}
