﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Tiles : NetworkBehaviour {	

	public enum Openings {
		UP,
		RIGHT,
		DOWN,
		LEFT
	};

    public enum TileShape
    {
        PATH,
        ROOM,
        EXIT
    };

	public enum TileType {
		CROSS,
		T_SHAPE,
		STRAIGHT,
		TURN,
		DEAD_END
	};

	public int x, y;
    public TileShape m_Shape;
    public TileType my_tile_type;
    public bool highlighted = false;
	public bool isTrap = false;
    public Openings[] my_openings;
    public int m_RotationNumber = 0;

	public LocalPlayerManager player_manager;

	protected void Start() {
		player_manager = GameObject.Find ("LocalPlayer").GetComponent<LocalPlayerManager>();
	}

	public void Update() {

		if (highlighted && Input.GetMouseButtonUp (0)) {

			// Check if moves available
			if (player_manager.move_available > 0) {
				
				// Check if next to player
				GameObject[,] board = player_manager.m_gameboard.board;
				Vector2 player_pos = player_manager.m_gameboard.m_BoardStruct.player_pos;
				GameObject player_tile = board[(int)player_pos.x, (int)player_pos.y];

				// X, Y-1 --> PLAYER BOTTOM
				if(x - player_pos.x == 0 && y - player_pos.y == 1) {
					for (int i = 0; i < my_openings.Length; i++) {
						if ((int)my_openings [i] == 2) {
							for (int j = 0; j < player_tile.GetComponent<Tiles>().my_openings.Length; j++) {
								if ((int)player_tile.GetComponent<Tiles>().my_openings[j] == 0) {
									player_manager.m_gameboard.pawn.transform.position = new Vector3 (x + x * player_manager.m_gameboard.offset, 0, y + y * player_manager.m_gameboard.offset);
									player_manager.m_gameboard.pawn.transform.SetParent (this.gameObject.transform);
									player_manager.m_gameboard.m_BoardStruct.player_pos = new Vector2 (x, y);
                                    player_manager.move_available--;
									break;
								}
							}
							break;						
						}
					}
				}
				// X-1, Y --> PLAYER LEFT
				else if(x - player_pos.x == 1 && y - player_pos.y == 0) {
					for (int i = 0; i < my_openings.Length; i++) {
						if ((int)my_openings [i] == 3) {
							for (int j = 0; j < player_tile.GetComponent<Tiles>().my_openings.Length; j++) {
								if ((int)player_tile.GetComponent<Tiles>().my_openings[j] == 1) {
									player_manager.m_gameboard.pawn.transform.position = new Vector3 (x + x * player_manager.m_gameboard.offset, 0, y + y * player_manager.m_gameboard.offset);	
									player_manager.m_gameboard.pawn.transform.SetParent (this.gameObject.transform);
									player_manager.m_gameboard.m_BoardStruct.player_pos = new Vector2 (x, y);	
                                    player_manager.move_available--;						
									break;
								}
							}
							break;	
						}
					}
				}
				// X, Y+1 --> PLAYER TOP
				else if(x - player_pos.x == 0 && y - player_pos.y == -1) {
					for (int i = 0; i < my_openings.Length; i++) {
						if ((int)my_openings [i] == 0) {
							for (int j = 0; j < player_tile.GetComponent<Tiles>().my_openings.Length; j++) {
								if ((int)player_tile.GetComponent<Tiles>().my_openings[j] == 2) {
									player_manager.m_gameboard.pawn.transform.position = new Vector3 (x + x * player_manager.m_gameboard.offset, 0, y + y * player_manager.m_gameboard.offset);	
									player_manager.m_gameboard.pawn.transform.SetParent (this.gameObject.transform);
									player_manager.m_gameboard.m_BoardStruct.player_pos = new Vector2 (x, y);		
                                    player_manager.move_available--;						
									break;
								}
							}
							break;	
						}
					}
				}
				// X+1, Y --> PLAYER RIGHT
				else if(x - player_pos.x == -1 && y - player_pos.y == 0) {
					for (int i = 0; i < my_openings.Length; i++) {
						if ((int)my_openings [i] == 1) {
							for (int j = 0; j < player_tile.GetComponent<Tiles>().my_openings.Length; j++) {
								if ((int)player_tile.GetComponent<Tiles>().my_openings[j] == 3) {
									player_manager.m_gameboard.pawn.transform.position = new Vector3 (x + x * player_manager.m_gameboard.offset, 0, y + y * player_manager.m_gameboard.offset);		
									player_manager.m_gameboard.pawn.transform.SetParent (this.gameObject.transform);
									player_manager.m_gameboard.m_BoardStruct.player_pos = new Vector2 (x, y);			
                                    player_manager.move_available--;				
									break;
								}
							}
							break;	
						}
					}
				}
				// X, Y --> CLICK ON PLAYER
				else if(x - player_pos.x == 0 && y - player_pos.y == 0) {	
					player_manager.m_gameboard.pawn.transform.position = new Vector3 (x + x * player_manager.m_gameboard.offset, 0, y + y * player_manager.m_gameboard.offset);		
					player_manager.m_gameboard.pawn.transform.SetParent (this.gameObject.transform);
					player_manager.m_gameboard.m_BoardStruct.player_pos = new Vector2 (x, y);			
					player_manager.move_available--;
                }
            }
		}

	}

	public void RotateTileLeft() {
		
		transform.Rotate (new Vector3 (0, -90, 0));

		for(int i = 0; i < my_openings.Length; i++) {
			int temp = (int)my_openings[i] - 1;
			if (temp < 0) temp = 3;
			my_openings[i] = (Openings)temp;
		}

	}

	public void RotateTileRight() {

		transform.Rotate (new Vector3 (0, 90, 0));

		for(int i = 0; i < my_openings.Length; i++) {
			int temp = (int)my_openings[i] + 1;
			if (temp > 3) temp = 0;
			my_openings[i] = (Openings)temp;
		}

        m_RotationNumber++;
        m_RotationNumber %= 4;
	}

	void OnMouseEnter() {
		highlighted = true;
		transform.localScale *=  0.95f;
	}

	void OnMouseExit() {
		highlighted = false;
		transform.localScale /=  0.95f;
	}

    public static void InstanciateTileOnGameObject(StructTile iNewTile, GameObject iObject)
    {
        switch(iNewTile.tileShape)
        {
            case TileShape.PATH:
                iObject.AddComponent<Tiles_Path>().SetupRoom(iNewTile.x, iNewTile.y, iNewTile.tileType, iNewTile.rotation);
                break;
            case TileShape.ROOM:
                iObject.AddComponent<Tiles_Room>().SetupRoom(iNewTile.x, iNewTile.y, iNewTile.tileType, iNewTile.rotation);
                break;
            case TileShape.EXIT:
                iObject.AddComponent<Tiles_Exit>();
                break;

        }
    }
}
