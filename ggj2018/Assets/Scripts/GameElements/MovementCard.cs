﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MovementCard : Card {
	
	public int m_MaxMovement;
	public TextMeshPro txt; 
	private int m_TilesNumber;
	LoadAssets load_assets;
	public bool highlighted = false;
	public LocalPlayerManager player_manager;

	void Start() {
		player_manager = GameObject.Find ("LocalPlayer").GetComponent<LocalPlayerManager>();
		load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();	
		transform.Rotate (new Vector3 (0, 180, 0));
	}

	public void SetNumber(int num) {
		load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();	
		m_TilesNumber = num;
		txt.text = "";
		GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.card_char[m_TilesNumber-1]);
	}

	void Update () {
		if (highlighted && Input.GetMouseButtonUp (0) && !card_played && !player_manager.card_played) {
			player_manager.move_available = m_TilesNumber;
			player_manager.card_played = true;
			card_played = true;

			transform.localScale = new Vector3 (0.2849f, 2.5763f, 0.2836f);
			transform.position = new Vector3 (6.63f, 0.4367f, 1.95f);
		}

		if (card_played && player_manager.move_available == 0) {
			player_manager.card_played = false;
			Destroy (this.gameObject);
		}

		if(card_played) txt.text = player_manager.move_available + "/" + m_TilesNumber;
	}

	void OnMouseEnter() {
		if (!player_manager.card_played) {
			highlighted = true;
			transform.localScale *=  0.95f;
		}
	}

	void OnMouseExit() {
		if (!player_manager.card_played) {
			highlighted = false;
			transform.localScale /= 0.95f;
		}

	}
}
