﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct StructGameBoard
{
    public StructTile[] tiles;
    public Vector2 player_pos;
}
