﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct StructTile{

    public int x, y;
    public bool isTrap;
    public Tiles.Openings[] my_openings;
    public Tiles.TileType tileType;
    public int rotation;
    public Tiles.TileShape tileShape;
}
