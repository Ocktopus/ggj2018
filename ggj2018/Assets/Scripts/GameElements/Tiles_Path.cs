﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiles_Path : Tiles {

	public int num_of_openings = 0;
	LoadAssets load_assets;

	// Gnerate a Path randomly
	public void GeneratePath() {
        m_Shape = TileShape.PATH;

        load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();
		// Create openings - initialisation commence de 0 (HAUT) à 3 (GAUCHE)
		int type_of_tile = Random.Range(0, 5); // TODO : A ponderer si on souhaiter (+) ou (-) d'une forme de tile
		my_tile_type = (TileType)type_of_tile;

		switch (my_tile_type) {
		case TileType.CROSS:
			num_of_openings = 4;
			my_openings = new Openings[] { (Openings)0, (Openings)1, (Openings)2, (Openings)3 };
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.path_textures[0]);
			break;
		case TileType.T_SHAPE:
			num_of_openings = 3;
			my_openings = new Openings[] { (Openings)0, (Openings)1, (Openings)2 };
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.path_textures [1]);
			break;
		case TileType.STRAIGHT:
			num_of_openings = 2;
			my_openings = new Openings[] { (Openings)0, (Openings)2 };
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.path_textures [2]);
			break;
		case TileType.TURN:
			num_of_openings = 2;
			my_openings = new Openings[] { (Openings)0, (Openings)1 };
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.path_textures [3]);	
			break;
		case TileType.DEAD_END:
			num_of_openings = 1;
			my_openings = new Openings[] { (Openings)0 };
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", load_assets.path_textures [4]);	
			break;
		default:
			break;
		}

		int rotate_random = Random.Range (0, 4);
		for (int i = 0; i < rotate_random; i++) {
			RotateTileRight ();
		}
		transform.Rotate (new Vector3 (0, 180, 0));
	}

	// Create a Room manually

	public void SetupRoom(int x, int y, TileType tile_type,  int num_of_rotate_right)
    {
        load_assets = GameObject.Find("LoadAssets").GetComponent<LoadAssets>();
        m_Shape = TileShape.ROOM;
        my_tile_type = tile_type;

        RefreshOpeningsAndTexture();
        RefreshRotation(num_of_rotate_right);
        
        this.x = x;
		this.y = y;
	}

    public void RefreshOpeningsAndTexture()
    {
        switch (my_tile_type)
        {
            case TileType.CROSS:
                {
                    my_openings = new Openings[4];
                    for (int i = 0; i < my_openings.Length; i++)
                    {
                        my_openings[i] = (Openings)i;
                    }
                    GetComponent<Renderer>().material.SetTexture("_MainTex", load_assets.path_textures[0]);
                    break;
                }
            case TileType.T_SHAPE:
                {
                    my_openings = new Openings[3];
                    for (int i = 0; i < my_openings.Length; i++)
                    {
                        my_openings[i] = (Openings)i;
                    }
                    GetComponent<Renderer>().material.SetTexture("_MainTex", load_assets.path_textures[2]);
                    break;
                }
            case TileType.STRAIGHT:
                {
                    my_openings = new Openings[2];
                    my_openings[0] = (Openings)0;
                    my_openings[1] = (Openings)2;
                    GetComponent<Renderer>().material.SetTexture("_MainTex", load_assets.path_textures[1]);
                    break;
                }
            case TileType.TURN:
                {
                    my_openings = new Openings[2];
                    for (int i = 0; i < my_openings.Length; i++)
                    {
                        my_openings[i] = (Openings)i;
                    }
                    GetComponent<Renderer>().material.SetTexture("_MainTex", load_assets.path_textures[3]);
                    break;
                }
            case TileType.DEAD_END:
                {
                    my_openings = new Openings[1];
                    my_openings[0] = (Openings)0;
                    GetComponent<Renderer>().material.SetTexture("_MainTex", load_assets.path_textures[4]);
                    break;
                }
        }
    }

    private void RefreshRotation(int num_of_rotate_right)
    {
        for (int i = 0; i < num_of_rotate_right; i++)
        {
            RotateTileRight();
        }
    }

}