﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPlayerManager : MonoBehaviour {
	

	public GameBoard m_gameboard;

	// Cards in hand
	public GameObject prefab_char_card_move;
	public GameObject prefab_char_card_slide;

	public GameObject prefab_world_card_rot;
	public int max_cards = 3;
	public float card_hand_offset = 1.2275f;
	public GameObject[] cards_in_hand;
	public Vector3[] card_pos;
	public bool card_played = false;

	// State
	public int move_available = 0;
	public int can_draw = 0;
	public int possible_action = 2;
	public bool waiting_for_slide = false;
	public int hp = 3;

	// Pool de cartes
	public enum Deck_Cards_Character {MOVE1, MOVE2, MOVE3, MOVE4};
	public enum Deck_Cards_World {SLIDE_ROW, SLIDE_COL, ROTATE};
	public List<Deck_Cards_Character> deck_character_pool;
	public List<Deck_Cards_World> deck_world_pool;
	public List<Deck_Cards_Character> deck_char;
	public List<Deck_Cards_World> deck_world;

	public int num_card_mov_1 = 3;
	public int num_card_mov_2 = 3;
	public int num_card_mov_3 = 2;
	public int num_card_mov_4 = 1;

	public int num_card_slide_row = 4;
	public int num_card_slide_col = 4;

	public int num_card_rot = 3;

	public GameObject win_txt;
	public GameObject vertical_slide_canvas;
	public GameObject horizontal_slide_canvas;

	// Use this for initialization
	void Start () {
		// Initialize decks 
		deck_character_pool = new List<Deck_Cards_Character> ();
		deck_world_pool = new List<Deck_Cards_World> ();
		deck_char = new List<Deck_Cards_Character> ();
		deck_world = new List<Deck_Cards_World> ();

		int num_of_char_cards = num_card_mov_1 + num_card_mov_2 + num_card_mov_3 + num_card_mov_4;
		for (int i = 0; i < num_of_char_cards; i++) {
			if (num_card_mov_1 > 0) {
				deck_character_pool.Add(Deck_Cards_Character.MOVE1);
				num_card_mov_1--;
			}
			else if (num_card_mov_2 > 0) {
				deck_character_pool.Add(Deck_Cards_Character.MOVE2);
				num_card_mov_2--;
			}
			else if (num_card_mov_3 > 0) {
				deck_character_pool.Add(Deck_Cards_Character.MOVE3);
				num_card_mov_3--;
			}
			else if (num_card_mov_4 > 0) {
				deck_character_pool.Add(Deck_Cards_Character.MOVE4);
				num_card_mov_4--;
			}
		}

		int num_of_world_cards = num_card_slide_row + num_card_slide_col + num_card_rot;
		for (int i = 0; i < num_of_world_cards; i++) {
			if (num_card_slide_row > 0) {
				deck_world_pool.Add(Deck_Cards_World.SLIDE_ROW);
				num_card_slide_row--;
			}
			else if (num_card_slide_col > 0) {
				deck_world_pool.Add(Deck_Cards_World.SLIDE_COL);
				num_card_slide_col--;
			}
			else if (num_card_rot > 0){
				deck_world_pool.Add(Deck_Cards_World.ROTATE);
				num_card_rot--;
			}
		}

		cards_in_hand = new GameObject[max_cards];

		FillAndShuffleDecks ();
		NewTurn ();
	}


	public bool new_turn = false;

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp (KeyCode.N))
			new_turn = true;

		// DEBUG -- Test New Turn
		if (new_turn) {
			NewTurn ();
			new_turn = false;
		}

	}

	public void DrawCharCard() {
		if (can_draw > 0) {
			if (deck_char.Count == 0)
				FillAndShuffleDecks ();
			for (int i = 0; i < max_cards; i++) {
				if (cards_in_hand [i] == null) {
					Deck_Cards_Character d = deck_char [0];
					deck_char.Remove (d);
					MovementCard c;
					Debug.Log (i + "   " + card_pos [i]);
					switch (d) {
					case Deck_Cards_Character.MOVE1:
						c = Instantiate (prefab_char_card_move, card_pos [i], Quaternion.identity).GetComponent<MovementCard> ();
						cards_in_hand [i] = c.gameObject;
						c.SetNumber (1);
						break;
					case Deck_Cards_Character.MOVE2:
						c = Instantiate (prefab_char_card_move, card_pos [i], Quaternion.identity).GetComponent<MovementCard> ();
						cards_in_hand [i] = c.gameObject;
						c.SetNumber (2);					
						break;
					case Deck_Cards_Character.MOVE3:
						c = Instantiate (prefab_char_card_move, card_pos [i], Quaternion.identity).GetComponent<MovementCard> ();
						cards_in_hand [i] = c.gameObject;
						c.SetNumber (3);					
						break;
					case Deck_Cards_Character.MOVE4:
						c = Instantiate (prefab_char_card_move, card_pos [i], Quaternion.identity).GetComponent<MovementCard> ();
						cards_in_hand [i] = c.gameObject;
						c.SetNumber (4);					
						break;
					}
					break;
				}
				can_draw--;
			}
		}
	}

	public void DrawWorldChar() {

		if (can_draw > 0) {
			if (deck_world.Count == 0) FillAndShuffleDecks ();
			for (int i = 0; i < max_cards; i++) {
				if (cards_in_hand [i] == null) {
					Deck_Cards_World d = deck_world [0];
					deck_world.Remove (d);
					SlideCard c;
					RotateCard r;
					switch (d) {
					case Deck_Cards_World.SLIDE_ROW:
						c = Instantiate (prefab_char_card_slide, card_pos [i], Quaternion.identity).GetComponent<SlideCard> ();
						cards_in_hand [i] = c.gameObject;
						c.SetDirection (0);
						break;
					case Deck_Cards_World.SLIDE_COL:
						c = Instantiate (prefab_char_card_slide, card_pos [i], Quaternion.identity).GetComponent<SlideCard> ();
						cards_in_hand [i] = c.gameObject;
						c.SetDirection (1);			
						break;
					case Deck_Cards_World.ROTATE:
						r = Instantiate (prefab_world_card_rot, card_pos [i], Quaternion.identity).GetComponent<RotateCard> ();
						cards_in_hand [i] = r.gameObject;
						break;
					}
					break;
				}
				can_draw--;
			}
		}
	}

	void FillAndShuffleDecks() {
		
		deck_char.Clear ();
		deck_char = new List<Deck_Cards_Character>(deck_character_pool);

		int n = deck_char.Count;
		while (n > 1) {
			n--;
			int k = Random.Range (0, n + 1);
			Deck_Cards_Character value = deck_char [k];
			deck_char [k] = deck_char [n];
			deck_char [n] = value;
		}		

		deck_world.Clear ();
		deck_world = new List<Deck_Cards_World>(deck_world_pool);

		n = deck_world.Count;
		while (n > 1) {
			n--;
			int k = Random.Range (0, n + 1);
			Deck_Cards_World value = deck_world [k];
			deck_world [k] = deck_world [n];
			deck_world [n] = value;
		}	
	}

	public void NewTurn() {

		// On resoud les tiles du board venant d'arriver
		Debug.Log(m_gameboard.board);
		if(m_gameboard.board[(int)m_gameboard.m_BoardStruct.player_pos.x, (int)m_gameboard.m_BoardStruct.player_pos.y].GetComponent<Tiles>().isTrap) {
			hp--;
		}

		// Le monde fait des trucs
		m_gameboard.GetComponent<ApplyWorldCard>().pickCard();

		// Reset draw
		int n = 3;
		for (int i = 0; i < cards_in_hand.Length; i++) {
			if (cards_in_hand [i] == null) {
				n--;
			}
		}
		can_draw = max_cards - n;

		// TODO : Reset actions
		possible_action = 2;
	}

	public void swapBoard() {
		// On resoud les tiles avant de swap de board
		if(m_gameboard.board[(int)m_gameboard.m_BoardStruct.player_pos.x, (int)m_gameboard.m_BoardStruct.player_pos.y].GetComponent<Tiles>().isTrap) {
			hp--;
		}

		// PASS BOARD
		// ????????????
	}
}
