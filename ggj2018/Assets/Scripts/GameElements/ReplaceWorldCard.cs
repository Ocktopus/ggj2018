﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaceWorldCard : ReplaceCard {

	public GameBoard m_Board;

	public int m_LineNumber;
	public int m_ColumnNumber;

	public int m_Rotation;

	public new void GenerateReplaceCard () {
		base.GenerateReplaceCard();
		m_LineNumber = Random.Range(0,m_Board.dim_x);
		m_ColumnNumber = Random.Range(0,m_Board.dim_y);
		m_Rotation = Random.Range(0,4);
	}
}
