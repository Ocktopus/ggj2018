﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaceCard : Card {
	
	public TypeOfPath m_Type;
	public List<int> m_Exit = new List<int>();
	public TypeOfTile m_TypeOfTile;
	public GameObject card_illustration;

	LoadAssets load_assets;


	public void GenerateReplaceCard(){

		load_assets = GameObject.Find ("LoadAssets").GetComponent<LoadAssets> ();

		m_Type = (TypeOfPath)Random.Range(0,System.Enum.GetNames(typeof(TypeOfPath)).Length);
		m_TypeOfTile = (TypeOfTile)Random.Range(0,System.Enum.GetNames(typeof(TypeOfTile)).Length);

		switch (m_Type)
		{
			case TypeOfPath.STRAIGHT:{
				m_Exit.Add(0);
				m_Exit.Add(2);
				if (m_TypeOfTile == TypeOfTile.PATH)
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.path_sprites [2];
				else
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.room_sprites  [2];
				break;
			}
			case TypeOfPath.T:{
				m_Exit.Add(0);
				m_Exit.Add(1);
				m_Exit.Add(2);
				if (m_TypeOfTile == TypeOfTile.PATH)
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.path_sprites  [1];
				else
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.room_sprites [1];
				break;
			}
			case TypeOfPath.CROSS:{
				m_Exit.Add(0);
				m_Exit.Add(1);
				m_Exit.Add(2);
				m_Exit.Add(3);
				if (m_TypeOfTile == TypeOfTile.PATH)
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.path_sprites  [0];
				else
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.room_sprites [0];
				break;
			}
			case TypeOfPath.TURN:{
				m_Exit.Add(0);
				m_Exit.Add(1);
				if (m_TypeOfTile == TypeOfTile.PATH)
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.path_sprites  [3];
				else
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.room_sprites [3];
				break;
			}
			case TypeOfPath.DEAD_END:{
				m_Exit.Add(0);
				if (m_TypeOfTile == TypeOfTile.PATH)
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.path_sprites  [4];
				else
					card_illustration.GetComponent<SpriteRenderer> ().sprite = load_assets.room_sprites [4];
				break;
			}
		}
	}
}
